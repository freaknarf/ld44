///upper rectangle
draw_set_alpha(1)
draw_set_halign(fa_left)
draw_set_colour(c_black)
rectH=128+64
draw_rectangle(view_xview[0],view_yview[0],view_xview[0]+view_wview[0],view_yview[0]+rectH,0)
draw_set_colour(c_white)

///HUD IS COMING -_-'
xStr1=32+str_off_x*1+view_xview[0]
xStr2=32+str_off_x*2+view_xview[0]
xStr3=32+str_off_x*3+view_xview[0]
yStr1=16+view_yview[0];
yStr2=16+str_off_x+view_yview[0];


//draw_money
//draw_text_white_outlined(view_xview[0]+32,view_yview[0]+32,"$BANK$:"+string(money*100)+"$/"+string(moneyGoal*100))
draw_text_white_outlined(32+view_xview[0]+32,view_yview[0]+32,string(money*100)+"$/"+string(moneyGoal*100))
draw_sprite_ext(sIcons,12, view_xview[0]+32,view_yview[0]+32,1,1,0,c_white,1)

///character HUD
if instance_exists(selected){
draw_text_white_outlined(xStr1-256+32,yStr1+64,string(selected.myname))

//state
draw_text_white_outlined(xStr1-256+32,yStr1+2*64,"State:"+string(selected.str))
///poo
str2=(selected.pooTime/selected.pooTimeMax)*100
str2=round(str2)
str=string(str2)+"%"
draw_text_white_outlined(xStr1+32,yStr1,str)
draw_sprite_ext(sIcons,13, xStr1,yStr1,1,1,0,c_white,1)

//str2=(selected.idleTime/selected.idleTimeMax)*100
//str="Idle"+string(str2)+"%"
//draw_text_white_outlined(xStr2,yStr2,str)


str2=selected.moral*100
str=string(str2)+"%"
draw_text_white_outlined(xStr2+32,yStr1,str)
draw_sprite_ext(sIcons,10,xStr2,yStr1,1,1,0,c_white,1)

//
str2=selected.skill*100
str="Skill"+string(str2)+"%"
draw_text_white_outlined(xStr2,yStr1+2*64,str)
//
str2=(selected.salary*100)
str="$"+string(str2)+"%"
draw_text_white_outlined(xStr3,yStr1+2*64,str)

///life
str2=selected.life
str=string(str2)+"%"
draw_text_white_outlined(xStr3+32,yStr1,str)
draw_sprite_ext(sIcons,4,xStr3,yStr1,1,1,0,c_white,1)

}
//draw_text_white_outlined(xStr3-256+32,yStr1+64,"Level:"+string(level))
