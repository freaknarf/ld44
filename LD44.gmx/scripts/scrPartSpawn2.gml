particle1 = part_type_create();
var xx=argument0
var yy=argument1
var ang=argument2


part_type_shape(particle1,pt_shape_line);

part_type_scale(particle1,.3,.3);
part_type_color2(particle1,c_green,c_green);

part_type_orientation(particle1,ang-10,ang+10,(1-irandom(2) )*0.10,0.20,1);
//part_type_blend(particle1,1);
part_type_life(particle1,40,60);
emitter1 = part_emitter_create(Sname);
part_emitter_region(Sname,emitter1,xx,xx,yy,yy,ps_shape_line,ps_distr_linear);
part_emitter_burst(Sname,emitter1,particle1,1);

