with oTarget{
    if instance_exists(selected)
    and target=selected {
        instance_destroy()
    }
}
