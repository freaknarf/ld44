if gameOver=1{
    draw_set_colour(c_black)
    draw_rectangle(0,room_height-64,room_width,room_height,0)
    draw_set_colour(c_white)
    draw_text_white_outlined(0,room_height-48,"Warning : low money")
}
if gameOver=2{
    draw_text_white_outlined(0,room_height-48,"Warning : bankrupt - game over")
    if alarm[11]=-1
    alarm[11]=5*room_speed
}

